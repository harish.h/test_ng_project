package Utility_comman_method;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_api_logs {

	public static void evidence_creator(File dir_name, String File_name, String endpoint, String requestBody,
			String responseBody) throws IOException {
		File newFile = new File(dir_name + "\\" + File_name +".txt");
		System.out.println("to save request and response body we have created a new file named : " + newFile.getName());
        
		FileWriter dataWriter=new FileWriter(newFile);
		dataWriter.write("Endpoint is :"+endpoint+"\n\n");
		dataWriter.write("RequestBody is :"+requestBody+"\n\n");
		dataWriter.write("responseBody is :"+responseBody);
		dataWriter.close();
	}
}
