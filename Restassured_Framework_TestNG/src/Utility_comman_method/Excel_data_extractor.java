package Utility_comman_method;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel_data_extractor {

	public static ArrayList<String> Excel_data_reader(String filename, String sheet_name, String tc_name)
			throws IOException {
		ArrayList<String> Arraydata = new ArrayList<String>();
		String project_dir = System.getProperty("user.dir");

		// step1: create the object of file input stream to locate the data file
		FileInputStream fis = new FileInputStream(project_dir + "\\Data_files\\" + filename + ".xlsx");

		// step2: create the XSSFWorkbook object to open the excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);

		// step3: fetch the no of sheets available in the excel file
		int count = wb.getNumberOfSheets();

		// step4: access the sheet as per the input sheet name
		for (int i = 0; i < count; i++) {
			String sheetname = wb.getSheetName(i);

			if (sheetname.equals(sheet_name)) {
				System.out.println(sheetname);
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> row = sheet.iterator();
				row.next();
				while (row.hasNext()) {
					Row datarow = row.next();
					String tcname = datarow.getCell(0).getStringCellValue();
					// System.out.println(tcname);
					if (tcname.equals(tcname)) {
						Iterator<Cell> cellvalues = datarow.iterator();
						while (cellvalues.hasNext()) {
							String testdata = cellvalues.next().getStringCellValue();
							// System.out.println(testdata);
							Arraydata.add(testdata);
						}
					}

				}
				break;
			}
		}
		wb.close();
		return Arraydata;

	}

}
