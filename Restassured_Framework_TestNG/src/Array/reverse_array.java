package Array;

public class reverse_array {

	public static void main(String[] args) {
		
		int[] input = {3,10,11,2};
		int[] output = new int[input.length];
		
		for(int i=input.length-1, j=0; i>=0; i--,j++)
		{
			output[j] = input[i];
		}
		
		for(int i:output)
		{
			System.out.println(i+" ");
		}

	}

}
