import io.restassured.RestAssured;

import io.restassured.path.xml.XmlPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class soap_reference_numbertodoller {

	public static void main(String[] args) {
		
		// step1 - declare base url

 			RestAssured.baseURI = "https://www.dataaccess.com";

				// step2 - declare request body

				String requestBody = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
						+ "   <soapenv:Header/>\r\n"
						+ "   <soapenv:Body>\r\n"
						+ "      <web:NumberToDollars>\r\n"
						+ "         <web:dNum>32.5</web:dNum>\r\n"
						+ "      </web:NumberToDollars>\r\n"
						+ "   </soapenv:Body>\r\n"
						+ "</soapenv:Envelope>";

				// step3 - trigger the api and fetch the response body

				String responseBody = given().header("Content-Type", "text/xml; charset=utf-8").body(requestBody).when()
						.post("/webservicesserver/NumberConversion.wso").then().extract().response().getBody().asString();
				
				//step4 - print the responsebody

				System.out.println(responseBody);
				
				//step5 - extract the requestbody parameter
				
				XmlPath Xml_res=new XmlPath(responseBody);
		    	String res_tag=Xml_res.getString("NumberToDollarsResult");
		    	System.out.println(res_tag);
		    	
		    	//step6 - validate the responsebody
		    	
		    	Assert.assertEquals(res_tag , "thirty two dollars and fifty cents");

	}

}
