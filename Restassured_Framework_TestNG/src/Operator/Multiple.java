package Operator;

public class Multiple { 
	
	public static void main(String[] args) { 
		
		int x=20;
		int y=10;
		
		int sum = x+y;
		int difference = x-y;
		int product = x*y;
		int quotient = x/y;
		int percentage = x%y;
		
		System.out.println("sum:" +sum);
		System.out.println("difference:" +difference);
		System.out.println("product:" +product);
		System.out.println("quotient:" +quotient);
		System.out.println("percentage:" +percentage);
	}

}
