package Operator;

public class Or_operator {
	public static void main(String[] args) { 
		int a=5;
		int b=10;
		if (a>b && b>a) { //false true
			System.out.println("first if");
		}
		if (a<b && b<a) { //true false
			System.out.println("second if");
		}
		if (b>a && a>a) { //true false
			System.out.println("third if");
		}
		if (a!=b && b>=a) { //true true
			System.out.println("forth if");
		}
		
		 a=6;
		 b=3;
		if (a>b || b>a) { //false true
			System.out.println("first if");
		}
		if (a<b || b<a) { //true false
			System.out.println("second if");
		}
		if (b>a || a>a) { //true false
			System.out.println("third if");
		}
		if (a!=b || b>=a) { //true true
			System.out.println("forth if");
		}
			
	}
	
}




