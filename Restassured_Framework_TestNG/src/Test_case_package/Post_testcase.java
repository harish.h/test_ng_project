package Test_case_package;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_comman_methods.comman_method_handle_api;
import Endpoint.Post_endpoint;
import Request_repository.Post_request_repository;
import Utility_comman_method.Handle_api_logs;
import Utility_comman_method.Handle_directory;
import io.restassured.path.json.JsonPath;

public class Post_testcase extends comman_method_handle_api {

	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;

	@BeforeTest

	public static void Test_Setup() throws IOException {
		log_dir = Handle_directory.create_log_directory("post_tc1_logs");
		requestBody = Post_request_repository.post_request_tc1();
		endpoint = Post_endpoint.post_endpoint_tc1();
	}

	@Test(description="::::::::executing the Post api and validating the responsebBody:::::::")
	public static void Post_executor() throws IOException {

		for (int i = 0; i < 5; i++) {
			int statuscode = Post_statusCode(requestBody, endpoint);
			System.out.println(statuscode);

			if (statuscode == 201) {
				String responseBody = post_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				Post_testcase.validator(requestBody, responseBody);
				break;
			} else {
				System.out.println("Expected statuscode not found,hence retrying");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_id = jsp_res.getString("id");
		String res_job = jsp_res.getString("job");
		String res_createdate = jsp_res.getString("createdAt");
		Instant timestamp = Instant.parse(res_createdate);  
	    res_createdate = timestamp.atZone(ZoneId.systemDefault()).toOffsetDateTime().toString();
		res_createdate = res_createdate.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdate, expecteddate);
	}

	@AfterTest
	public static void Test_Teardown() throws IOException {
		Handle_api_logs.evidence_creator(log_dir, "post_tc1", endpoint, requestBody, responseBody);

	}
}