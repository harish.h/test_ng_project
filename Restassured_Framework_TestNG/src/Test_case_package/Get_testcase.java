package Test_case_package;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_comman_methods.comman_method_handle_api;
import Endpoint.Get_endpoint;
import Utility_comman_method.Handle_api_logs;
import Utility_comman_method.Handle_directory;
import io.restassured.path.json.JsonPath;

public class Get_testcase extends comman_method_handle_api {
	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responseBody;

	@BeforeTest
	public static void Test_Setup() {
		log_dir = Handle_directory.create_log_directory("get_tc1_logs");
		requestBody = null;
		endpoint = Get_endpoint.Get_endpoint_tc1();
	}

	@Test
	public static void Get_executor() throws IOException {

		for (int i = 0; i < 5; i++) {
			int statusCode = Get_statusCode(endpoint);
			System.out.println(statusCode);

			if (statusCode == 200) {
				String responseBody = get_responseBody(endpoint);
				System.out.println(responseBody);
				Get_testcase.validator(responseBody);
				break;
			} else {
				System.out.println("expected statuscode not found,hence retrying");
			}
		}
	}

	public static void validator(String responseBody) {
		JsonPath jsp_res = new JsonPath(responseBody);
		JSONObject array_res = new JSONObject(responseBody);
		JSONArray dataarray = array_res.getJSONArray("data");

		int expected_id[] = { 1, 2, 3, 4, 5, 6 };
		String expected_firstname[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
		String expected_lastname[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };
		String expected_email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };

		int count = dataarray.length();

		for (int i = 0; i < count; i++) {
			int res_id = dataarray.getJSONObject(i).getInt("id");
			System.out.println(res_id);
			int exp_id = expected_id[i];

			String res_firstname = dataarray.getJSONObject(i).getString("first_name");
			String res_lastname = dataarray.getJSONObject(i).getString("last_name");
			String res_email = dataarray.getJSONObject(i).getString("email");

			String exp_firstname = expected_firstname[i];
			String exp_lastname = expected_lastname[i];
			String exp_email = expected_email[i];

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_firstname, exp_firstname);
			Assert.assertEquals(res_lastname, exp_lastname);
			Assert.assertEquals(res_email, exp_email);
		}

	}

	@AfterTest
	public static void Test_Teardown() throws IOException {
		Handle_api_logs.evidence_creator(log_dir, "get_tc1", endpoint, requestBody, responseBody);
	}

}