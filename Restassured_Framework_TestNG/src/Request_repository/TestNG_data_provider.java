package Request_repository;

import org.testng.annotations.DataProvider;

public class TestNG_data_provider {
       
	@DataProvider()
	public Object [][] post_data_provider() {
		return new Object[][]
			{
				{"morpheus","leader"},				
				{"Harish","QA"},
				{"Srikanth","Developer"}
			};
	}
}
