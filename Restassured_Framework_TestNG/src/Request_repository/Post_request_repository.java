package Request_repository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_comman_method.Excel_data_extractor;

public class Post_request_repository { 
	
	public static String post_request_tc1() throws IOException { 
		ArrayList<String> Data=Excel_data_extractor.Excel_data_reader("test_data","post_api","post_tc1");
		//System.out.println(Data);
		String name = Data.get(1);
		String job = Data.get(2);
		
		String requestBody = "{\r\n" 
				+"\"name\": \""+name+"\",\r\n"
				+ "\"job\": \""+job+"\"\r\n" + "}";
		
		return requestBody;  
	}

}
